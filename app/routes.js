"use strict";

module.exports = function (app, db) {
    
    var bAuth = require('./auth')(db)
    var q = require('./query')
    var r = require('./responses')
    
    
    app.get('/',bAuth, function (req, res) {
        res.send('..')
        /*q.getUserByPhone('+71112223344').then(function (data) {
            console.log(data)
        })*/
    })

    
    app.get('/profile', bAuth, function (req, res) {
        q.getUserByPhone(req.auth.user)
                .then(function (data) {
                    var result = {}
                    var fields = ['id', 'username', 'name', 'group', 'phone']
                    for (var i = 0; i < fields.length; i++) {
                        result[fields[i]] = data[fields[i]]
                    }
                    res.json(result)
                })
                .catch(function (err) {
                    r.getError(res, err)
                })
    })

    app.get('/tasks', bAuth, function (req, res) {
        q.getTasks()
                .then(function (data) {
                    if (data) {
                        res.json(data)
                    } else {
                        r.noFound(res)
                    }
                })
                .catch(function (err) {
                    r.getError(res, err)
                })
    })

    app.post('/tracking', bAuth, function (req, res) {
        if (req.body.location) {
            var location = req.body.location
            q.getUserByPhone(req.auth.user)
                    .then(function (data) {
                        if (data) {
                            var uid = data.id
                            db.query("INSERT INTO users_track(uid, location) VALUES ($1 ,$2)", [uid, location])
                                    .then(function () {
                                        //console.log(data)
                                        //console.log("insert track")
                                    })
                        } else {
                            noFound(res)
                        }
                    })
                    .catch(function (err) {
                        getError(res, err)
                    })
            sendOk(res)
        } else {
            wrongParams(res)
        }
    })

    app.post('/completeTask', bAuth, function (req, res) {
        if (req.body.id && req.body.data) {
            var req_data = req.body.data
            var tid = req.body.id
            q.getUserByPhone(req.auth.user)
                    .then(function (data) {
                        var uid = data.id
                        q.getTaskById(tid)
                                .then(function (data) {
                                    if (data) {
                                        db.query("INSERT INTO complete_tasks( uid, tid, data ) VALUES ($1 ,$2, $3)", [uid, tid, req_data])
                                                .then(function () {
                                                    //console.log(data)
                                                    //console.log("insert complete_tasks")
                                                })
                                        sendOk(res)
                                    } else {
                                        noFound(res)
                                    }
                                })
                                .catch(function (err) {
                                    getError(res, err)
                                })

                    })
                    .catch(function (err) {
                        getError(res, err)
                    })
        } else {
            wrongParams(res)
        }
    })

}