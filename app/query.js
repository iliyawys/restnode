"use strict"

var db = require('./db')

module.exports.getUserByPhone = function getUserByPhone(phone) {
    return db.one("SELECT * from users where phone=$1", [phone])
}
module.exports.getTaskById = function getTaskById(id) {
    return db.one("SELECT * from tasks where id=$1", [id])
}
module.exports.getTasks = function(){
    return db.any("SELECT * from tasks")
}

