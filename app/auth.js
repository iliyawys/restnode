"use strict";

module.exports = function (db) {

    const basicAuth = require('express-basic-auth')

    return basicAuth({
        authorizer: customAuthorizer,
        unauthorizedResponse: getUnauthorizedResponse,
        authorizeAsync: true
    })

    function getUnauthorizedResponse(req) {
        return req.auth ? ('Credentials ' + req.auth.user + ' rejected') : 'No credentials provided'
    }

    function customAuthorizer(phone, password, cb) {

        var bcrypt = require('bcryptjs')

        db.oneOrNone("SELECT * from users where phone=$1", [phone])
                .then(function (data) {
                    if (data) {
                        var hash = data.password
                        hash = hash.replace(/^\$2y(.+)$/i, '\$2a$1')
                        bcrypt.compare(password, hash, function (err, r) {
                            if (r) {
                                return cb(null, true)
                            } else {
                                return cb(null, false)
                            }
                        });
                    } else {
                        return cb(null, false)
                    }
                })

                .catch(function (err) {
                    return cb(null, false)
                });
        return true
    }

}