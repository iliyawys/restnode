"use strict";

//init
const express = require('express')
const bodyParser = require('body-parser')
const app = express()

app.use(bodyParser.json())
//app.use(bodyParser.urlencoded({ extended: false }))

const db = require('./app/db')
require('./app/routes')(app, db);

module.exports = app;